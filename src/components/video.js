import React from'react';


const Video =({youtubeId}) =>{
    return(
        <div className="embed-responsive embed-responsive-21by9">
            <iframe  title="myFrame" className="embed-responsive-item" src={`https://www.youtube.com/embed/${youtubeId}`}/>
        </div>
    )
};

export default Video;
