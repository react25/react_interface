import  React,{Component}  from 'react'

class  SearchBar extends Component{
    constructor(props){
        super(props)
            this.state= {searText:"",placeholder:"tapez votre  film" }

    }
    handlechange(event){
       this.setState({searText:event.target.value})
    }
    handleClick(event){
      this.props.callback(this.state.searText);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-8 input-group">
                    <input type="text" className="form-control input-lg"  onChange={this.handlechange.bind(this)} placeholder={this.placeholder}/>
                    <span className="input-group-btn">
                        <button className="btn btn-info" onClick={this.handleClick.bind(this)}>GO</button>
                    </span>

                </div>
            </div>
        )
    }
}
export  default  SearchBar;