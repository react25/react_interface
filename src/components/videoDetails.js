import  React  from  'react'

const VideoDetail = ({title,description, releaseDate}) =>{
    return (
        <div>
            <h1>
                {title}
            </h1><br/>
            <i>Sorti le: {releaseDate}</i>
            <p>{description}</p>
        </div>
    )
}

export default VideoDetail;