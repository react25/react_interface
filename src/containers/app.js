import React,{Component}  from 'react';
import VideoDetail from "../components/videoDetails";
import SearchBar from "../components/search";
import VideoList from "./videoList";
import axios from 'axios';
import Video from "../components/video"

const API_KEY= "api_key=07690eff71d9a1f4c84252c8dd511977"
const API_END_POINT = "https://api.themoviedb.org/3/"
const POPULAR_MOVIES_URL = "discover/movie?language=en&sort_by=popularity.desc&include_adult=false&append_to_response=images"
const SEARCH_URL= "search/movie?language=?fr&include_adult=false"

class App extends Component {
    constructor (props){
        super(props)
        this.state ={
            movieList:{},
            currentMovie:{}
        }


    }
    UNSAFE_componentWillMount () {
          this.initMovies()
    }

    initMovies(){
        axios.get(`${API_END_POINT}${POPULAR_MOVIES_URL}&${API_KEY}`).then(function (response){
            this.setState({movieList:response.data.results.slice(1,6),currentMovie:response.data.results[0] },function(){
                this.applyVideoToCurrentMovie()
            })

        }.bind(this));
    }

    applyVideoToCurrentMovie(){
            axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}?${API_KEY}&append_to_response=videos&include_adult=false`).then(function (response){
                const  youtubeKey = response.data.videos.results[0].key;

                let  newCurrentMovieState =this.state.currentMovie;
                newCurrentMovieState.youtubeId =youtubeKey;
                this.setState({
                    currentMovie:newCurrentMovieState
                });
            }.bind(this));
    }
    receiveCalllBack(movie){
        this.setState({currentMovie:movie},function(){
            this.applyVideoToCurrentMovie()
            this.setRecommandation()
        })
    }
    onClickSearchText(searchText){
        if(searchText) {
            axios.get(`${API_END_POINT}${SEARCH_URL}&${API_KEY}&query=${searchText}`).then(function (response) {
                if(response.data && response.data.results[0]) {
                    if (response.data.results[0].id !== this.state.currentMovie.id)
                        this.setState({currentMovie: response.data.results[0]}, function () {
                            this.applyVideoToCurrentMovie()
                            this.setRecommandation()
                        })
                }
            }.bind(this));
        }

    }

    setRecommandation(){
        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}/recommendations?${API_KEY}&language=fr&include_adult=false`).then(function (response){
            this.setState({movieList:response.data.results.slice(0,5)})
        }.bind(this));

    }

    render() {
        const  lenghtMovie= this.state.movieList.length;
        let renderVideoList;
        if (lenghtMovie >= 5) {
            renderVideoList = <VideoList movieList={this.state.movieList} callback={this.receiveCalllBack.bind(this)} />
        }


        return (<div>
                    <div className="search_bar">
                    <SearchBar callback={this.onClickSearchText.bind(this)}/>
                    </div>
                    <div className="row">
                        <div className="col-md-8">
                            <Video youtubeId={this.state.currentMovie.youtubeId}/>
                            <VideoDetail title={this.state.currentMovie.title}   description ={this.state.currentMovie.overview} releaseDate={this.state.currentMovie.release_date}/>
                        </div>
                        <div className="col-md-4">
                            {renderVideoList}
                        </div>
                    </div>

            </div>
        )

    }

}

export  default App;